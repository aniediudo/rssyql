angular.module("school-connect.directives", [])
.directive("itemThumbnail", function (itemImageLookup) {
   
    return function (scope, element, attr) {
        var kill = scope.$watch(attr.itemThumbnail, function (item) {
            if (item) {
                kill();
                itemImageLookup(item)
                .then(function (data) {
                    if (data && !(data.match(/^\(/))) {
                        attr.$set("src", data);
                    }else {
                        attr.$set("src", faker.Image.nature());
                    }
                }, function () {
                    attr.$set("src", faker.Image.nature());
                });
            }
            
        });
    };
    
})

.directive("itemBannerImage", function (itemImageLookup) {
   
    return function (scope, element, attr) {
        var kill = scope.$watch(attr.itemBannerImage, function (item) {
            if (item) {
                kill();
                itemImageLookup(item)
                .then(function (data) {
                    if (data && !(data.match(/^\(/))) {
                        attr.$set("src", data);
                    }else {
                        attr.$set("src", faker.Image.nature());
                    }
                }, function () {
                    attr.$set("src", faker.Image.nature());
                });
            }
        });
    };
    
});
