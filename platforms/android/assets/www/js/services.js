angular.module("school-connect.services", [])

.constant("mockResponse", false)

.constant("API_YQL", "https://query.yahooapis.com/v1/public/yql")

.factory("Store", function ($window) {
   var cache = {};
    
    return {
      get : function (key) {
          if ($window.localStorage) {
              return $window.localStorage.getItem(key);
          }
          return cache[key];
      },
      set : function (key, value) {
          if ($window.localStorage) {
              return $window.localStorage.setItem(key, value);
          }
          return cache[key] = value;
      },
      getObject : function (key, defaultValue) {
          if ($window.localStorage) {
              var o = $window.localStorage.getItem(key);
              if (o) {
                  return angular.fromJson(o);
              }
              return defaultValue;
          }
          return (cache[key] || defaultValue);
      },
      setObject : function (key, value) {
          if ($window.localStorage) {
              return $window.localStorage.setItem(key, angular.toJson(value));
          }
          return cache[key] = value;
      }    
    };
    
})
.factory("Chats", function () {
    
    return {
      all : function () {
          return [];
      }
    };
})
.factory("itemImageLookup", function ($http, $q, Store, API_YQL) {
    
    var getters = [];
    
    getters.push(function scrapeDocumentOG(item, done) {
        var content = item.content || '';
        if (content) {
            var PATTERN_OG = /rel\=["']og\:image/,
                PATTERN_IMG = /\<img\s+src\=["'](\S+)['"]/i;
            
            var orgLocation = PATTERN_OG.exec(content);
            if (orgLocation) {
                PATTERN_IMG.lastIndex = orgLocation.index + orgLocation[0].length;
                var imageMatch = PATTERN_IMG.exec(content);
                if (imageMatch) {
                    return done(null, imageImatch[1]);
                }
            }
        }
        done(null, null);
    });
    
    getters.push(function scrapeMeta(item, done) {
        
        $http.jsonp(API_YQL, {
            params : {
                q : "select * from html where url='" + (item.link) + "' and xpath='//meta|//img'",
                callback : "JSON_CALLBACK",
                format : "json"
            }
        }).success(function (data) {
            var r = /og\:image/,
                contentGrabber = /content=['"](\S)["']/;
            
            
            var getMetaList = function (results) {
                if (angular.isArray(results)) {
                     return results;
                }
                return (results.meta || []);
            };
            
            var results = data.query.results;
            if (results) {
                var meta = _.find(results.meta || [], function (m) {
                    if (angular.isString(m)) {
                        return r.test(m);
                    }
                    return (r.test(m.property || m.name));
                });
                if (meta) {
                    var content = angular.isString(meta) ? contentGrabber.exec(meta)[1] : meta.content;
                    return done(null, content);
                }
                if (results.img) {
                    if (angular.isArray(results.img)) {
                        return done(null, results.img[0].src);
                    }
                    return done(null, results.img.src);
                }
            }
            done(null, null);
        }).error(function(err) {
            done(err);       
        
        });
        
    });
    
    
    getters.push(function inmlineHtmlImage(item, done) {
        PATTERN_IMG = /\<img\s+src\=["'](\S+)['"]/i;
        var content = item.content || '';
        var match = PATTERN_IMG.exec(content);
        if (match) {
            return done(null, match[1]);
        }
        done(null, null);
    });
    
    
    var tryGetImage = function (index, item, done) {
        if (getters.length > index) {
            getters[index](item, function (err, data) {
                if (err) {
                    return done(err);
                }
                if (data) {
                    return done(null, data);
                }
                
                tryGetImage(index + 1, item, done);
            });
        }else {
            done(new Error("Not found"));
        }
    };
    
    
    
    
    return function (item, defaultPath) {
        var deferred = $q.defer();
        var cacheKey = "__image__" + item.link;
        var data = Store.get(cacheKey) || '';
        if (data) {
            deferred.resolve(data);
        }else {
            tryGetImage(0, item, function (err, data) {
                if (err) {
                    Store.set(cacheKey, '(empty)');
                    deferred.reject(err);
                }else {
                    //set a key to trigger cache hit on subsequent requests
                    Store.set(cacheKey, data || '(empty)');   
                    deferred.resolve(data);
                }
            });
        }
        return deferred.promise;
    };
})
.service("YQL", function ($http, $q, API_YQL) {
    var processorsPipeLine = [];
    
    processorsPipeLine.push(function  extractItems(data) {
        
        var count = data.query.count;
        
        data = data.query.results.item;
        if (data) {
            if (_.isArray(data)) {
                return data;
            }
            return [
                data
            ];
        }
        return [];
    });
    
    processorsPipeLine.push(function assignIds(data) {
        _.each(data, function (item, i) {
            item.id = i + 1;
        });
        return data;
    });
    
    processorsPipeLine.push(function createSnippets(data) {
        _.each(data, function (item) {
            if (_.isUndefined(item.contentSnippet)) {
                item.contentSnippet = s.prune(item.description, 150);
            }
            item.content = item.description;
            delete item.description;
        });
        return data;
    });
    
    var postProcess = function (data) {
        _.each(processorsPipeLine, function (fn) {
            data = fn(data);
        });
        return data;
    };
    
    var optionsMakers = [
        function (destination, source) {
            var url = source.url || source.link;
            destination.q = "select * from rss where url=\"" + url + "\"";
            return destination;
        },
        function (destination) {
            destination.format = "json";
            destination.callback = "JSON_CALLBACK ";
            return destination;
        },
        function (destination, source) {
            if (_.isObject(source.query)) {
                return _.extend(destination, options.query);
            }
            return destination;
        }
    ];
    
    
    var toLocalOptions = function (source) {
        var lo = {};
        _.each(optionsMakers, function (fn) {
            lo = fn(lo, source);
        });
        return lo;
    };
    
    
    return {
      fetch : function (options) {
          var deferred = $q.defer();
          $http.jsonp(API_YQL, {
              params : toLocalOptions(options)
          })
          .success(function (data) {
              if (data.error) {
                  deferred.reject(data.error);
              }else {
                  deferred.resolve(postProcess(data));
              }
          })
          .error(function (err) {
              deferred.reject(err);
          });
          return deferred.promise;
      } 
    };
})

.factory("mockFeedFactory", function ($q) {
    var count = 15;
    
    
    var randomItem = function (id) {
        var description = faker.Lorem.paragraphs(3);
        return {
            id : id,
            contentSnippet : s.prune(description, 150),
            content : description,
            pubDate : faker.Date.past(4),
            title : faker.Lorem.sentence()
        };
    };
    
    var Factory = function (count) {
        this.items = [];
        for (var j = 1; j <= count; j++) {
            this.items.push(randomItem(j));
        }
    };
    
    Factory.prototype = {
        available : function () {
            var deferred = $q.defer();
            
            deferred.resolve(this.items);
            
            return deferred.promise;
        },
        item : function (id) {
            var deferred = $q.defer();
            deferred.resolve(_.findWhere(this.items, {id : id}));
            return deferred.promise;
        }
    };
    
    return function () {
        return new Factory(_.random(7, 15));
    };
})

.factory("factorySource", function (mockResponse, mockFeedFactory, feedFactory) {
    return function () {
        return  (mockResponse ? mockFeedFactory : feedFactory);
    };
})

.factory("feedFactory", function (Store, YQL, $q) {
    var isOld = function (cacheKey) {
        
        var lastUpdated = Store.get(cacheKey);
        
        if (lastUpdated) {
            lastUpdated = new Date(parseInt(lastUpdated, 10));
            var now = new Date();
            if (now.getFullYear() !== lastUpdated.getFullYear()) {
                return true;
            }
            
            if (now.getMonth() !== lastUpdated.getMonth()) {
                return true;
            }
            
            return (now.getDate() !== lastUpdated.getDate());
        }
        
        return true;
    };
    
    
    var passThru = function (funcs, data) {
        _.each(funcs, function (fn) {
            data = fn(data);
        });
        return data;
    };
    
    
    var Factory = function (options) {
        this.postProcessors = [];
        this.preProcessors = [];
        this.options = options;
    };
    
    var lastOptions;
    
    Factory.prototype = {
        available : function (refresh) {
            var options = _.clone(this.options);
            if (_.isObject(refresh)) {
                options = _.extend(options, refresh);
                refresh = !_.isEqual(options, lastOptions);
            }
            var defered = $q.defer(),
                self = this,
                dateCacheKey = this.options.cacheKey + "__last_fetched";
          if (refresh === true || isOld(dateCacheKey)) {
              lastOptions = options;
              if (self.preProcessors.length > 0) {
                  options = passThru(self.preProcessors, options);
              }
              YQL.fetch(options).then(function (data) {
                  if (self.postProcessors.length>0) {
                      data = passThru(self.postProcessors, data);
                  }
                  Store.setObject(self.options.cacheKey, data);
                  Store.set(dateCacheKey, String(Date.now()));
                  defered.resolve(data);
              }, function (err) {
                  defered.resolve(Store.getObject(self.options.cacheKey, []));
//                  if (console) {
//                      console.error(err);
//                  }
                  //defered.reject(err);
              });
          }else {
              defered.resolve(Store.getObject(self.options.cacheKey, []));
          }
          return defered.promise;
        },
        item : function (id) {
            var defered = $q.defer();
            var d = Store.getObject(this.options.cacheKey, []);
            defered.resolve(_.findWhere(d, {id : id}));
            return defered.promise;
        }
    };
    
    return function (options) {
        return new Factory(options);
    };
    
})
.factory("Announcement", function (factorySource) {
    var factory = factorySource()({
        url : "http://genesis.ibomconnection.com/category/newsletter/feed/",
        cacheKey : "sc-cache-announcements"
    });
    return factory;
})
.factory("News", function (factorySource) {
    
    var factory = factorySource()({
        url : "http://genesis.ibomconnection.com/category/news/feed/",
        cacheKey : "sc-cache-news"
    });
    return factory;
    
//    var KEY_LAST_NEWS_UPDATE = "sc-cache-news-last-update",
//        KEY_NEWS = "sc-cache-news",
//        FEED_URL = "http://genesis.ibomconnection.com/category/news/feed/";
//    
//    var isOld = function () {
//        var lastUpdated = Store.get(KEY_LAST_NEWS_UPDATE);
//        if (lastUpdated) {
//            lastUpdated = new Date(parseInt(lastUpdated, 10));
//            var now = new Date();
//            if (now.getFullYear() !== lastUpdated.getFullYear()) {
//                return true;
//            }
//            
//            if (now.getMonth() !== lastUpdated.getMonth()) {
//                return true;
//            }
//            
//            return (now.getDate() !== lastUpdated.getDate());
//        }
//        
//        return true;
//    };
//  var getImpl = function (refresh) {
//      var defered = $q.defer();
//      if (isOld() || refresh === true) {
//          YQL.fetch({
//              url : FEED_URL
//          }).then(function (data) {
//              Store.setObject(KEY_NEWS, data);
//              Store.set(KEY_LAST_NEWS_UPDATE, String(Date.now()));
//              defered.resolve(data);
//          }, function (err) {
//              defered.reject(err);
//          });
//      }else {
//          defered.resolve(Store.getObject(KEY_NEWS, []));
//      }
//      return defered.promise;
//  };
//    
//    var getItemImpl = function (id) {
//        var defered = $q.defer();
//        var d = Store.getObject(KEY_NEWS, []);
//        defered.resolve(_.findWhere(d, {id : id}));
//        return defered.promise;
//    };
//    
//    
//    
//    return {
//        available : getImpl,
//        item : getItemImpl
//    };
});