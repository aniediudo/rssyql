angular.module('school-connect.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  
  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  }
})

.controller("RootTabsCtrl", function ($scope, $ionicActionSheet) {
    $scope.share = function (item, title) {
        var hideSheet = $ionicActionSheet.show({
             buttons: [
               { text: 'SMS' },
               { text: 'Email' }
             ],
             titleText: title,
             cancelText: 'Cancel',
             cancel: function() {
                  // add cancel code..
                },
             buttonClicked: function(index) {
               return true;
             }
           });
    };
    
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
})
.controller("NewsCtrl", function ($scope, News) {
    $scope.refresh = function (reload) {
        $scope.loading = true;
        News.available(reload)
        .then(function (items) {
         $scope.loading = false;
         $scope.items = items;   
        }, function (err) {
            $scope.loading = false;
            $scope.errorMessage = "Failed to fetch available news. I think you are offline.";
        });
    };
    
    $scope.refresh(false);
})
.controller("AnnouncementsCtrl", function ($scope, Announcement) {
    
    
    $scope.refresh = function (reload) {
        $scope.loading = true;
        Announcement.available(reload)
        .then(function (items) {
         $scope.loading = false;
         $scope.items = items;   
        }, function (err) {
            $scope.loading = false;
            $scope.errorMessage = "I think you are not connected to the internet as I couldn't fetch annoucements";
        });
    };
    
    $scope.refresh(false);
})
.controller("NewsDetailsCtrl", function ($scope, $stateParams, News) {
    News.item(parseInt($stateParams.id, 10)).
    then(function (item) {
        $scope.title = _.isObject(item) ? item.title : '';
        $scope.item = item;
    });
})
.controller("AnnouncementDetailsCtrl", function ($scope, $stateParams, Announcement) {
    Announcement.item(parseInt($stateParams.id, 10)).
    then(function (item) {
        $scope.title = _.isObject(item) ? item.title : '';
        $scope.item = item;
    });    
});




