angular.module("school-connect.filters", [])
.filter("fromNow", function () {
    return function (date) {
        return moment(date).fromNow();
    };
})
.filter("toDate", function () {
    return function (date) {
        if (_.isString(date)) {
            date = new Date(date);
        }
        return date;    
    };
    
});