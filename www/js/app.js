// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('school-connect', ['ionic', 'school-connect.controllers',
                                  'school-connect.services',
                                  'school-connect.directives',
                                 'school-connect.filters'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "templates/tabs.html",
    controller : "RootTabsCtrl"  
  })

  // Each tab has its own nav history stack:

  .state('tab.news', {
    url: '/news',
    views: {
      'tab-dash': {
        templateUrl: 'templates/news/index.html',
        controller: 'NewsCtrl'
      }
    }
  })
  
  .state('tab.news-details', {
      url: '/news/details/:id',
      views: {
        'tab-dash': {
          templateUrl: 'templates/news/details.html',
          controller: 'NewsDetailsCtrl'
        }
      }
    })

  .state('tab.announcements', {
      url: '/announcements',
      views: {
        'tab-chats': {
          templateUrl: 'templates/announcements/index.html',
          controller: 'AnnouncementsCtrl'
        }
      }
    })
    
  
  .state('tab.announcement-details', {
      url: '/announcements/details/:id',
      views: {
        'tab-chats': {
          templateUrl: 'templates/announcements/details.html',
          controller: 'AnnouncementDetailsCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/news');

});
